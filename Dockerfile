# Use the official PHP image as the base image
FROM php:8.2.11-apache

USER root

WORKDIR /var/www/html
ADD . /var/www/html

COPY .env.example .env

# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    zip \
    unzip \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    && docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd

# Install Composer globally
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install PHP dependencies
RUN composer install --optimize-autoloader --no-dev

# Set permissions for Laravel
RUN chown -R www-data:www-data /var/www/html/storage /var/www/html/bootstrap/cache
RUN chmod -R 775 /var/www/html/storage /var/www/html/bootstrap/cache && a2enmod rewrite
RUN php artisan storage:link
RUN php artisan key:generate
# # Expose port 9000
# EXPOSE 9000

# # Start PHP-FPM server
# CMD ["php-fpm"]